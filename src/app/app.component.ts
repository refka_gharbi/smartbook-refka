import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  
  title = 'admin-template';

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {

    /*let isloggedin: string ;
    let loggedUtilisateur: string;

    isloggedin = localStorage.getItem('isloggedIn');
    loggedUtilisateur = localStorage.getItem('loggedUtilisateur');

    if (isloggedin! = "true" || !loggedUtilisateur)
      this.router.navigate(['/login']);
    else
      this.authService.setLoggedUserFromLocalStorage(loggedUtilisateur);*/
}
onLogout(){
  this.authService.logout();
}

 
}
