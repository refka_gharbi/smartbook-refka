import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLecteurComponent } from './add-lecteur.component';

describe('AddLecteurComponent', () => {
  let component: AddLecteurComponent;
  let fixture: ComponentFixture<AddLecteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddLecteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLecteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
